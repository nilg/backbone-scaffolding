define([
  'backbone'
], function(Backbone) {
  
  // example contact model
  var Contact = Backbone.Model.extend({

    // set the model id, which will be used for url 
    idAttribute: "contactId",

    defaults: {
      "name": "Contact Name",
      "photoUrl": "images/noPhoto.png"
    }
  });

  return Contact;
});