define([
  'backbone'
], function(Backbone) {
  
  // single account instance
  var userAccount;

  var Account = Backbone.Model.extend({

    idAttribute: "accountId",
    
    urlRoot: '/account',

    defaults: {
      "photoUrl": "images/noPhoto.png"
    },

    validation: {

      // example validation rules
      accountId: {
        required: true
      },
      firstName: {
        minLength: 3
      },
      lastName: {
        minLength: 3
      },
      email: {
        required: true,
        pattern: 'email',
        msg: 'Please enter a valid email'
      }
    }
  });

  Account.getInstance = function() {
    return userAccount;
  };

  Account.setAccount = function(account) {
    if (typeof account === 'object')
      userAccount = new Account(account);
  };

  Account.fetchAccount = function(fn) {
    if (userAccount === undefined) {
      userAccount = new Account();
    }
    userAccount.fetch({
      success: function() {
        fn(userAccount);
      },
      error: function() {
        userAccount.destroy();
        userAccount = undefined;
        fn(null);
      }
    });
  };

  return Account;
});