define([
  'hbs!templates/header/header',
  'models/account',
  'backbone_mbinder'
], function(headerTemplate, Account, ModelBinder) {

  var headerView = Backbone.View.extend({

    el: '.header',

    render: function() {
      
      // set model instance
      this.model = Account.getInstance();

      $(this.el).html(headerTemplate);

      /*
       * Model-View binding
       * 
       * All the model attributes are bound to html 'name' properties in the template
       * For example 'firstName' attribute is bound to name="firstName"
       */
      this.modelBinder = new ModelBinder();
      this.modelBinder.bind(this.model, this.el);
    },

    clean: function() {
      this.modelBinder.unbind();
    }
  });
  
  return headerView;
});
