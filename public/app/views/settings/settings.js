define([
  'hbs!templates/settings/settings',
  'models/account'
], function(settingsTemplate, Account) {

  var settingsView = Backbone.View.extend({
    el: '.main-container',

    render: function() {

      // set model instance
      this.model = Account.getInstance();

      this.$el.html(settingsTemplate);

      /*
       * Model validation
       *
       * Bind model attribute validation and implement callbacks
       * Model attribute is mapped to form element 'name'
       */
      Backbone.Validation.bind(this, {
        valid: function(view, attr) {
          // valid
        },
        invalid: function(view, attr, error) {
          alert(error);
        }
      });
    },

    // form submit event
    events: {
      'click #submit': 'updateModel'
    },

    updateModel: function(e) {

      e.preventDefault();

      // serialize form data
      var data = Backbone.Syphon.serialize(this);

      // set form data to model which triggers validation
      if (this.model.set(data))
        this.model.save();
    },

    clean: function() {
      Backbone.Validation.unbind(this);
    }
  });

  return settingsView;
});