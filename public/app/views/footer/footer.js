define([
  'hbs!templates/footer/footer'
], function(footerTemplate) {
  
  var footerView = Backbone.View.extend({
    
    el: '.footer',
    
    render: function() {
      $(this.el).html(footerTemplate);
    }
  });

  return footerView;
});
