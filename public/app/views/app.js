define([
  'vm',
  'text!templates/layout.html'
], function(Vm, layoutTemplate) {

 var appView = Backbone.View.extend({

    el: '#main', // append this view to the #main id

    render: function() {
      var that = this;

      // render layour template
      this.$el.html(layoutTemplate);
      
      // render header and footer
      require(['views/header/header'], function(HeaderView) {
        var headerView = Vm.create(that, 'HeaderView', HeaderView);
        headerView.render();
      });
      
      require(['views/footer/footer'], function(FooterView) {
        var footerView = Vm.create(that, 'FooterView', FooterView);
        footerView.render();
      });
    }
  });

 return appView;
});
