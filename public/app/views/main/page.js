define([
  'hbs!templates/main/page'
], function(mainPageTemplate) {

  var mainPage = Backbone.View.extend({
    el: '.main-container',

    render: function() {

      // send parameters to handlebars template
      var params = {
        name: "stranger"
      };

      this.$el.html(mainPageTemplate(params));
    }
  });
  
  return mainPage;
});
