/*
 * backbone-scaffolding web app
 * 
 * @author Nil Gradisnik <nil.gradisnik@gmail.com>
 */

require.config({

  paths: {

    // Major libraries
    jquery: 'lib/jquery/jquery',
    underscore: 'lib/underscore/underscore',
    backbone: 'lib/backbone/backbone',

    // Backbone Model/Collecction binder
    // - https://github.com/theironcook/Backbone.ModelBinder
    backbone_mbinder: 'lib/backbone/backbone.model.binder',
    //backbone_cbinder: 'lib/backbone/backbone.collection.binder',

    // Backbone validation plugin
    // - https://github.com/thedersen/backbone.validation
    backbone_validation: 'lib/backbone/backbone.validation',

    // Backbone form serialization plugin
    // - https://github.com/derickbailey/backbone.syphon
    backbone_syphon: 'lib/backbone/backbone.syphon',

    // RequireJS text plugin (use 'text!' prefix to render plain html files)
    text: 'lib/require/text',

    // Handlebars templating plugin
    // https://github.com/SlexAxton/require-handlebars-plugin
    hbs: 'lib/handlebars/hbs', // use 'hbs!' prefix to render handlebars templates
    handlebars: 'lib/handlebars/handlebars',
    i18nprecompile : "lib/handlebars/i18nprecompile",
    json2 : "lib/handlebars/json2",

    // templates location
    templates: '../templates'
  },

  // Handlebars config
  hbs: {

    disableI18n: false, // localization file is read from 'app/template/i18n'
    disableHelpers: true // handlebars helper files are read from 'app/template/helpers' (this does not work for some reason)

    //templateExtension: "html" // default is hbs
  }
});

// kick off the app
require([
  'backbone',
  'vm',
  'views/app',
  'router',
  'models/account',

  // load plugins
  'backbone_validation',
  'backbone_syphon'
], function(Backbone, Vm, AppView, Router, Account) {

  // read account data from index.html
  if (window.UserAccount)
    Account.setAccount(UserAccount);

  // create main application layout
  var appView = Vm.create({}, 'AppView', AppView);
  appView.render();

  // initialize backbone router
  Router.initialize({appView: appView});
});
