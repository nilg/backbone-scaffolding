define([
  'backbone',
  'models/contact'
], function(Backbone, contactModel){

  // example collection which holds contact model
  var Contacts = Backbone.Collection.extend({

    idAttribute: "contactId",

    model: contactModel
  });
  
  // fetch all contacts
  Contacts.getAll = function(fn) {
    var contacts = new Contacts();
  
    contacts.fetch({
      success: function() {
        fn.call(this, contacts);
      },
      error: function() {
        contacts = null;
        fn.call(this, null);
      }
    });
  };

  return Contacts;
});
