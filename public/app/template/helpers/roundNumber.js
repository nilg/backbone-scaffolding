define([
  'handlebars'
], function(Handlebars) {

  // more info: https://github.com/SlexAxton/require-handlebars-plugin
  function roundNumber(context, options) {
    return Math.round(context);
  }
  Handlebars.registerHelper('roundNumber', roundNumber);
  return roundNumber;
});