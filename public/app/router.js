define([
  'backbone',
  'vm'
], function (Backbone, Vm) {

  var AppRouter = Backbone.Router.extend({

    routes: {

      // pages
      'settings': 'settings',

      // default - catch all
      '*actions': 'defaultAction'
    }
  });

  var initialize = function(options) {

    var appView = options.appView;
    var router = new AppRouter(options);
    
    // create and render the appropriate page
    router.on('route:settings', function() {
      require(['views/settings/settings'], function(SettingsPage) {
        var settingsPage = Vm.create(appView, 'SettingsPage', SettingsPage);
        settingsPage.render();
      });
    });
    
    router.on('route:defaultAction', function(actions) {
      require(['views/main/page'], function(MainPage) {
        var mainPage = Vm.create(appView, 'MainPage', MainPage);
        mainPage.render();
      });
    });
    
    Backbone.history.start();
  };
  
  return {
    initialize: initialize
  };
});
