## backbone-scaffolding

This is a fairly simple web application using Backbone.js
It's mean to be a helpful starting point for new projects

* Author: Nil Gradisnik
* Contact [nil.gradisnik](mailto:nil.gradisnik@gmail.com)
* Twitter: [@ghaefb](http://twitter.com/ghaefb)

## Technologies in use

* [RequireJS](http://requirejs.org/)
* [Backbone.js](http://http://backbonejs.org/)
* [Underscore](http://http://underscorejs.org/)
* [Handlebars](http://handlebarsjs.com/)
* [Stylus](http://learnboost.github.com/stylus/)

## Requirements

### CSS compilation:
  [Stylus](http://learnboost.github.com/stylus/) compiles .styl files

### Build project (minified javascript files, one css file):
  [Node.js](http://nodejs.org/) for running build/build.sh script
  [UglifyJS](https://github.com/mishoo/UglifyJS2) to minify/uglify js files
