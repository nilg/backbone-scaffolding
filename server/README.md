# This is a minimal Node.js server for testing the web client.

## Dependencies 

1. [Express](http://expressjs.com) for web app layer. 

2. [Stylus](http://learnboost.github.com/stylus/) styl-to-css compiler.

3. [Handlebars](https://github.com/donpark/hbs) templating engine

Use npm to install these modules.

```
npm install
```

## Project structure
For this to work with expresso-web the file/folder structure should be:

```
backbone-skeleton/
  server/
    server.js
  public/
    ...
```